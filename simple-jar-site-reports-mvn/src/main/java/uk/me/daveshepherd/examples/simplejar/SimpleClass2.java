package uk.me.daveshepherd.examples.simplejar;

public class SimpleClass2 {

	/**
	 * Returns the length of the input value
	 * 
	 * @param value
	 * @return The length of the input
	 */
	public int simpleMethod(String value) {
		String someValue = "some value";
		return value.length();
	}

	/**
	 * Returns the length of the input value
	 * 
	 * @param value
	 * @return
	 */
	public int simpleMethod1(String value) {
		return value.length();
	}

	/**
	 * Returns the length of the input value
	 * 
	 * @param value
	 * @return
	 */
	public int simpleMethod2(String value) {
		return value.length();
	}

	/**
	 * Returns the length of the input value
	 * 
	 * @param value
	 * @return
	 */
	public int simpleMethod3(String value) {
		return value.length();
	}

	/**
	 * Returns the length of the input value
	 * 
	 * @param value
	 * @return
	 */
	public int simpleMethod4(String value) {
		return value.length();
	}

	/**
	 * Returns the length of the input value
	 * 
	 * @param value
	 * @return
	 */
	public int simpleMethod5(String value) {
		return value.length();
	}

	/**
	 * Returns the length of the input value
	 * 
	 * @param value
	 * @return
	 */
	public int simpleMethod6(String value) {
		return value.length();
	}

	/**
	 * Returns the length of the input value
	 * 
	 * @param value
	 * @return
	 */
	public int simpleMethod7(String value) {
		return value.length();
	}

	/**
	 * Returns the length of the input value
	 * 
	 * @param value
	 * @return
	 */
	public int simpleMethod8(String value) {
		return value.length();
	}
}
